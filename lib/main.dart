// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: Text('Flutter Firebase Navigation Example'),
          ),
          body: NavigationScreen(),
        ),
      ),
    );
  }
}

class NavigationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TopBarContents(),
        SectionList(),
      ],
    );
  }
}

class TopBarContents extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue,
      padding: EdgeInsets.all(16.0),
      child: Column(
        children: [
          ElevatedButton(
            onPressed: () => scrollToSection(context, 0),
            child: Text('Section 1'),
          ),
          ElevatedButton(
            onPressed: () => scrollToSection(context, 1),
            child: Text('Section 2'),
          ),
          ElevatedButton(
            onPressed: () => scrollToSection(context, 2),
            child: Text('Section 3'),
          ),
        ],
      ),
    );
  }

  void scrollToSection(BuildContext context, int index) {
    final controller = DefaultTabController.of(context);
    controller.animateTo(index);
  }
}

class SectionList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: TabBarView(
        children: [
          SectionPage(number: 1, title: 'Section 1', color: Colors.red),
          SectionPage(number: 2, title: 'Section 2', color: Colors.green),
          SectionPage(number: 3, title: 'Section 3', color: Colors.blue),
        ],
      ),
    );
  }
}

class SectionPage extends StatelessWidget {
  final int number;
  final String title;
  final Color color;

  const SectionPage({
    Key? key,
    required this.number,
    required this.title,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
      child: Center(
        child: Text(
          '$title Content',
          style: TextStyle(fontSize: 24, color: Colors.white),
        ),
      ),
    );
  }
}
